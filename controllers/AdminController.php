<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use app\models\Notice;


class AdminController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['admin/index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    return Yii::$app->response->redirect(['/auth']);
                }
            ]
        ];
    }

    public function actionIndex()
    {
        // простую проверку на админа
        if(Yii::$app->user->getId() <> 100) {
            $this->goHome();
        }

        Yii::$app->view->title = 'Простая админка уведомлений';

        return $this->render('index', [
            'Notices' => Notice::find()->orderBy('id')->all()
        ]);
    }

}