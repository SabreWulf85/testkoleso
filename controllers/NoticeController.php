<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Notice;
use yii\helpers\Url;
use yii\filters\AccessControl;

class NoticeController extends Controller {

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['index', 'add', 'edit', 'delete', 'post'],
                'rules' => [
                    [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['?', '@'],
                    ],
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
                'denyCallback' => function($rule, $action) {
                    throw new \Exception('У вас нет доступа к этой странице');
                }
            ]
        ];
    }


    public function actionIndex()
    {
        // далаем сессию чтобы записывать уведомления которые уже были показаны
        $session = Yii::$app->session;
        $notNoticeId = $session->has('userNotice') ? $session->get('userNotice'): [];

        // берем одну новую запись, которую ещё не видел пользователеь
        $notice = Notice::find()->where(['not in','id', $notNoticeId])->one();
        if(!empty($notice->id) && !in_array($notice->id, $notNoticeId)) {
            array_push($notNoticeId, $notice->id);
            $session->set('userNotice', ($notNoticeId));

            // отметим что просмотрели уведомление
            $notice->countviews++;
            $notice->save();

            // показываем сообщение
            die(json_encode([
                'result' => true,
                'html' => $this->renderPartial('view', compact('notice'))
            ]));

        } else {
            // удалим и будем показывать все заново
            $session->destroy();
        }
    }


    public function actionAdd() {

        return $this->render('form', [
            'create' => true,
            'category' => $this->getCategory()
        ]);
    }

    public function actionEdit() {

        if(empty(Yii::$app->request->get()['id'])) {
            return $this->redirect('/admin');
        }

        $notice = Notice::findOne(Yii::$app->request->get()['id']);
        if(empty($notice->id)) {
            return $this->redirect(Url::to(['notice/add']));
        }

        return $this->render('form', [
            'create' => false,
            'notice' => $notice,
            'category' => $this->getCategory()
        ]);
    }

    public function actionPost() {

        if(Yii::$app->user->getId() <> 100) {
            throw new \Exception('У вас нет доступа к этой странице');
        }

        if(Yii::$app->request->post()) {
            $post = Yii::$app->request->post();

            if(empty(trim($post['title']) || empty($post['text']))) {
                throw new \Exception('Пустое поле');
            }

            $transaction = Yii::$app->db->beginTransaction();
            try {
                $notice = !empty($post['id']) ? Notice::findOne($post['id']) : new Notice();

                // если категория по умолчанию не прописана
                $catid = !empty($post['catid']) ?? 1;

                $notice->title = mb_substr($post['title'], 0, 240);
                $notice->text  = $post['text'];
                $notice->catid = $catid;
                $notice->save();

                $transaction->commit();
                return $this->redirect( Url::to( !empty($post['id']) ? ['notice/edit', 'id' => $post['id']] : '/admin/' ));

            } catch(\Exception $e) {
                $transaction->rollBack();
                throw $e;
            } catch(\Throwable $e) {
                $transaction->rollBack();
                throw $e;
            }
        }
        return $this->goHome();
    }

    public function actionDelete() {

        if(Yii::$app->user->getId() <> 100) {
            throw new \Exception('У вас нет доступа к этой странице');
        }

        if(!empty(Yii::$app->request->get()['id'])) {
            $notice = Notice::findOne(Yii::$app->request->get()['id']);
            if(!empty($notice)) {
                $notice->delete();
            }
        }
        return $this->redirect('/admin');
    }

    public function getCategory() {
        $category = [];
        foreach (Yii::$app->db->createCommand('select * from category')->queryAll() as $rows) {
            $category[$rows['id']] = $rows['name'];
        }
        return $category;
    }
}