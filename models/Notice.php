<?php

namespace app\models;

use yii\db\ActiveRecord;

class Notice extends ActiveRecord
{
    public static function tableName() {
        return 'notice';
    }
}