$(document).ready(function() {

    const NOTICE = '.page > .notice';

    window.notice = {

        show: function () {
            $(NOTICE).fadeIn();
        },

        hide: function () {
            $(NOTICE).fadeOut();
        },

        close: function(_this) {
            _this.closest('.notice').fadeOut();
        },

        handlerJson : function (url, data, handleData) {
            return $.ajax({type: 'post', url:url, dataType:'json', data:data, success:handleData});
        },

        load: function () {
            notice.handlerJson('/notice', {}, function (output) {
                if(output.result) {
                    $(NOTICE).html(output.html).fadeIn();
                    notice.fnDelay(function () {
                        notice.hide();
                        notice.fnDelay(function () {
                            notice.load();
                        }, 1000);
                    }, 5000);
                }
            });
        },

        fnDelay : function() {
            var timer = 0;
            return function(callback, ms) {
                clearTimeout(timer);
                timer = setTimeout(callback, ms);
            };
        }()
    };

    $(document).on('click', 'span.close', function() {
        notice.close($(this));
    });

    $(document).on('click', '.checkNotice', function() {
        notice.load();
    });


    $(document).on('click', 'input[name=auth]', function() {


        console.log('Ready');

        var form = $('form.loginform');
        var data = form.serialize();
        form.fadeTo(500, 0);

        /*
         setTimeout(function() {
         form.fadeTo(500, 1);
         $('form.loginform span.error').fadeIn();
         }, 1000);
         */

        $.ajax({
            type: 'post', url: '/auth/logon', dataType: 'json',
            data: data,
            success: function() {
                location.reload();
            },
            error: function(e) {
            }
        });
    });
});