-- ----------------------------
-- Table structure for category
-- ----------------------------
DROP TABLE IF EXISTS "public"."category";
CREATE TABLE "public"."category" (
"id" int4 DEFAULT nextval('category_id_seq'::regclass) NOT NULL,
"name" varchar(32) COLLATE "default"
)
WITH (OIDS=FALSE)

;

-- ----------------------------
-- Records of category
-- ----------------------------
INSERT INTO "public"."category" VALUES ('1', 'Автомобили');
INSERT INTO "public"."category" VALUES ('2', 'Космос');
INSERT INTO "public"."category" VALUES ('3', 'Природа');
INSERT INTO "public"."category" VALUES ('4', 'Наука');
INSERT INTO "public"."category" VALUES ('5', 'Девушки');

-- ----------------------------
-- Alter Sequences Owned By 
-- ----------------------------

-- ----------------------------
-- Primary Key structure for table category
-- ----------------------------
ALTER TABLE "public"."category" ADD PRIMARY KEY ("id");
