<?php use yii\helpers\Html; ?>
<div style="padding: 10px; text-align: right ">
    [+] <?= Html::a('Добавить новое уведомление', ['/notice/add']) ?>
</div>
<table class="table">
    <thead>
        <th>id</th>
        <th>Дата создания</th>
        <th>Заголовок</th>
        <th>Просмотров</th>
        <th>-</th>
    </thead>
    <tbody>
    <?php foreach($Notices as $rows): ?>
    <tr>
        <td><?= $rows->id ?></td>
        <td><?= Yii::$app->formatter->asDate($rows->ctime, 'php:d.m.Y H:i'); ?></td>
        <td>
            <?= Html::a($rows->title, ['/notice/edit', 'id' => $rows->id]) ?>
            <p><?= $rows->text ?></p>
        </td>
        <td style="text-align: center"><?= $rows->countviews ?></td>
    </tr>
    <?php endforeach ?>
    </tbody>
</table>

