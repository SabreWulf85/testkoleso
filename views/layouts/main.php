<?php
use yii\helpers\Html;
use app\assets\AppAsset;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

    <!-- Styles -->
    <style>
        html, body {
            background-color: #eee;
            color: #636b6f;
            font-family: 'Nunito', sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-panel {
            height: 50px;
            position: absolute;
            top: 0;
            text-align: center;
        }

        .top-panel__header {
            width: 98%;
            margin: 0 auto;
            max-width: 1450px;
        }

        .top-panel > .login {
            width: 98%;
            margin: 0 auto;
        }

        .login > .head {
            text-align: left;
            margin: 0 10px;
        }

        .login-form {
            float: right;
        }

        .main-container {
            width: 100%;
            position: absolute;
            top: 55px;
        }

        .main-container__general {
            text-align: center;
            opacity: 0;
            transition: 0.1s;
            animation: show 2s 1;
            animation-fill-mode: forwards;
            animation-delay: 0.1s;
            max-width: 1450px;
            width: 100%;
            margin: 0 auto;
        }

        @keyframes show {
            0% {opacity:0;}
            100% {opacity:1;}
        }

        .content {
            float: left;
            width: 100%;
        }

        .content_block .page {
            background-color: #fff;
            min-height: 60vh;
            text-align: left;
            position: relative;
        }

        .footer {
            position: fixed;
            height: 50px;
            bottom: 0;
            text-align: center;
        }

        .tb {
            background-color: #3CB371;
            width: 100%;
            left: 0;
            color: #fff;
            line-height: 50px;
            font-size: 24px;
        }

        .notice {
            position: fixed;
            bottom: 55px;
            max-width: 1450px;
            width: 100%;
            height: 150px;
            display: none;
        }

        .n-block {
            border: 1px solid #ccc;
            background-color: #F8F8FF;
            height: 150px;
            width: 100%;
            position: relative;
        }

        .n-block span, .n-block .content {
            padding: 10px;
        }

        .n-block span {
            font-size: larger;
            font-weight: 700;
        }

        .n-block .content {
            width: 90%;
        }

        .n-block .control {
            position: absolute;
            bottom: 5px;
            right: 5px;
            left: 0;
            font-size: 11px;
            text-align: right;
        }

        .close {
            position: absolute;
            right: 0;
            top: 0;
            width: 32px;
            height: 32px;
            opacity: 0.3;
        }
        .close:hover {
            opacity: 1;
        }
        .close:before, .close:after {
            position: absolute;
            left: 15px;
            content: ' ';
            height: 20px;
            width: 2px;
            background-color: #333;
        }
        .close:before {
            transform: rotate(45deg);
        }
        .close:after {
            transform: rotate(-45deg);
        }

        .hide {
            display: none;
        }


        .table { width: 100%; font-size: 14px; border-spacing: 0; }
        .table thead th { background-color: #F5F5F5; border-top: 1px solid rgba(0,0,0,.12); }
        .table thead th .t-right { text-align: right }
        .table thead th {
            font-size: 12px;
            padding: 5px 11px;
            font-weight: 500;
            color: rgba(0,0,0,.54);
            border-bottom: 1px solid rgba(0,0,0,.12);
            white-space: nowrap;
        }
        .table tr:hover td { color: #000; background: #E3F2FD; }
        .table thead th:last-child { padding-right: 10px; text-align: right; border-right: 1px solid rgba(0,0,0,.12); }
        .table thead th:first-child { padding-left: 10px; border-left: 1px solid rgba(0,0,0,.12); }
        .table tbody td { padding: 7px 10px; }
        .table tbody tr { cursor: pointer; outline: 0; }

    </style>
</head>
<body>
<?php $this->beginBody() ?>
<div class="flex-center position-ref full-height">
    <div class="top-panel tb">
        <div class="top-panel__header">
            <div class="login">
                <div class="head">
                    <a href="/" style="text-decoration:none; color: #fff">home</a> |
                    <?php if (!Yii::$app->user->isGuest): ?>
                        <?= Yii::$app->user->identity->username ?> -> (<a href="/auth/logout">logoff</a>)
                    <?php else: ?>
                        <a href="/auth/" style="text-decoration:none; color: #fff">авторизация</a>
                    <?php endif ?>
                </div>
            </div>
        </div>
    </div>
    <div class="main-container">
        <div class="main-container__general">
            <div class="content">
                <div class="content_block">
                    <div class="page">
                        <?= $content ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer tb">
        footer
    </div>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>