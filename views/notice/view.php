<div class="n-block">
    <div class="header">
        <span><?= $notice->title ?></span>
        <a href="javascript:void(0)">
            <span class="close"></span>
        </a>
    </div>
    <div class="content">
        <?= $notice->text ?>
    </div>
    <div class="control">
        <span>Просмотров (<?= $notice->countviews ?>)</span>
    </div>
</div>