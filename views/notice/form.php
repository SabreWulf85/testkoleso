<style>
    * {box-sizing: border-box}

    .container { padding: 16px; }

    input[type=text], input[type=password],  textarea, select {
        width: 100%;
        padding: 15px;
        margin: 5px 0 22px 0;
        display: inline-block;
        border: none;
        background: #f1f1f1;
    }

    input[type=text]:focus, input[type=password]:focus {
        background-color: #ddd;
        outline: none;
    }

    hr {
        border: 1px solid #f1f1f1;
        margin-bottom: 25px;
    }

    .appendNotice {
        background-color: blue;
        color: white;
        padding: 16px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        width: 10%;
        opacity: 0.9;
    }

    .appendNotice:hover { opacity:1; }
</style>
<div>
    <form action="/notice/post" method="post">
        <div class="container">
            <h1><?= $create ? 'Добавление нового уведомления' : 'Обновить уведомление' ?></h1>
            <p>Заполните все поля</p>
            <hr>
            <label for="email"><b>Заголовок (не более 240 символов)</b></label>
            <?= yii\helpers\BaseHtml::input('text', 'title', $create ? '' : $notice->title, ['placeholder' => 'Заголовок', 'required' => true]) ?>

            <label for="text"><b>Основной текст</b></label>
            <textarea placeholder="напишите основной текст уведомления" name="text" required><?= $create ? '' : $notice->text ?></textarea>
            <label for="category"><b>Укажите категорию</b></label>
            <?= yii\helpers\BaseHtml::DropDownList('catid', $create ? '' : $notice->catid, $category, ['prompt'=>'- Выберите категорию']) ?>
            <hr>
            <button type="submit" class="appendNotice"><?= $create ? 'Добавить' : 'Обновить' ?></button>

            <?php if (!$create): ?>
                <?= yii\helpers\Html::a('Удалить это сообщение', ['/notice/delete', 'id' => $notice->id], ['style' => 'float: right']) ?>
                <?= yii\helpers\BaseHtml::input('hidden', 'id', $notice->id) ?>
            <?php endif ?>
            <hr>
            <?= yii\helpers\Html::a('Все уведомления', ['/admin']) ?>
        </div>
    </form>
</div>