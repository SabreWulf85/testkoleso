<?php if (!Yii::$app->user->isGuest && Yii::$app->user->getId() == 100): ?>
    <?= yii\helpers\Html::a('Вы зашли как админ, тут можно работать с уведомлениями', ['/admin']) ?>
<?php endif ?>

<div class="notice"></div>
<?= $this->registerJsFile('js/client.js', ['depends' => 'yii\web\YiiAsset' ]) ?>